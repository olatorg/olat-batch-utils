# OLAT Batch Utils

[![License](https://img.shields.io/badge/License-Apache--2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

This module contains utilities to manage Spring Batch.

## Usage

1. Add the following dependency to your project's `pom.xml`.

   ```xml
   <dependency>
     <groupId>org.olat</groupId>
     <artifactId>olat-batch-utils</artifactId>
     <version>${olat-batch-utils.version}</version>
   </dependency>
   ```

## Development

1. Clone the source code.

   ```shell
   git clone git@gitlab.com:olatorg/olat-batch-utils.git
   cd olat-batch-utils
   ```

2. Build project.

   ```shell
   ./mvnw verify
   ```

## License

This project is Open Source software released under
the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).
