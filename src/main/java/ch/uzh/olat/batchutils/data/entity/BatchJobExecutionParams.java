/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.data.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Access to Spring batch table by JPA.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Entity
@Table(name = "batch_job_execution_params")
@IdClass(BatchJobExecutionParams.PrimaryKey.class)
@Getter
@Setter
public class BatchJobExecutionParams {

  @Id
  @ManyToOne(optional = false)
  @JoinColumn(name = "job_execution_id")
  private BatchJobExecution batchJobExecution;

  @Id
  @Column(name = "key_name", nullable = false)
  private String keyName;

  @Column(name = "date_val")
  private LocalDateTime dateVal;

  @Getter
  @Setter
  public static class PrimaryKey implements Serializable {

    private Long batchJobExecution;
    private String keyName;

    // Used by JPA
    @SuppressWarnings("unused")
    public PrimaryKey() {}

    // Used by JPA
    @SuppressWarnings("unused")
    public PrimaryKey(Long batchJobExecution, String keyName) {
      this.batchJobExecution = batchJobExecution;
      this.keyName = keyName;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      PrimaryKey that = (PrimaryKey) o;
      return batchJobExecution.equals(that.batchJobExecution) && keyName.equals(that.keyName);
    }

    @Override
    public int hashCode() {
      return Objects.hash(batchJobExecution, keyName);
    }
  }
}
