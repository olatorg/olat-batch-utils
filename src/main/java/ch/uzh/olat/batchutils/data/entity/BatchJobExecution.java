/*
 * Copyright 2022 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.uzh.olat.batchutils.data.entity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Access to Spring batch table by JPA.
 *
 * @author Martin Schraner
 * @since 1.0
 */
@Entity
@Table(name = "batch_job_execution")
@Getter
@Setter
public class BatchJobExecution {

  @Id
  @Column(name = "job_execution_id")
  private long jobExecutionId;

  @ManyToOne
  @JoinColumn(name = "job_instance_id", nullable = false)
  private BatchJobInstance batchJobInstance;

  @OneToMany(mappedBy = "batchJobExecution")
  private final Set<BatchStepExecution> batchStepExecutions = new HashSet<>();

  @Column(name = "start_time")
  private LocalDateTime startTime;

  @Column(name = "end_time")
  private LocalDateTime endTime;

  @Column(name = "status")
  private String status;

  @Column(name = "exit_code")
  private String exitCode;

  @Column(name = "exit_message")
  private String exitMessage;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BatchJobExecution that = (BatchJobExecution) o;
    return jobExecutionId == that.jobExecutionId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(jobExecutionId);
  }
}
