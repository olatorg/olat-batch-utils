CREATE TABLE IF NOT EXISTS batch_job_instance
(
    job_instance_id            BIGINT NOT NULL,
    version                    BIGINT,
    job_name                   VARCHAR(100),
    job_key                    VARCHAR(32),
    PRIMARY KEY (job_instance_id)
);

INSERT INTO batch_job_instance(job_instance_id, version, job_name, job_key)
    VALUES (100001, 0, 'job-a', 'abcedfg');
INSERT INTO batch_job_instance(job_instance_id, version, job_name, job_key)
    VALUES (100002, 0, 'job-b', 'hijklmn');
INSERT INTO batch_job_instance(job_instance_id, version, job_name, job_key)
    VALUES (100003, 0, 'job-b', 'opqrstu');
INSERT INTO batch_job_instance(job_instance_id, version, job_name, job_key)
    VALUES (100004, 0, 'job-b', 'vwxyzab');
INSERT INTO batch_job_instance(job_instance_id, version, job_name, job_key)
    VALUES (100005, 0, 'job-a', 'cdefghi');
INSERT INTO batch_job_instance(job_instance_id, version, job_name, job_key)
    VALUES (100006, 0, 'job-a', 'jklmnop');
INSERT INTO batch_job_instance(job_instance_id, version, job_name, job_key)
    VALUES (100007, 0, 'job-a', 'qrstuvw');
INSERT INTO batch_job_instance(job_instance_id, version, job_name, job_key)
    VALUES (100008, 0, 'job-a', 'xyzabcd');

CREATE TABLE IF NOT EXISTS batch_job_execution
(
    job_execution_id           BIGINT NOT NULL,
    version                    BIGINT,
    job_instance_id            BIGINT NOT NULL,
    create_time                TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    start_time                 TIMESTAMP WITHOUT TIME ZONE,
    end_time                   TIMESTAMP WITHOUT TIME ZONE,
    status                     VARCHAR(10),
    exit_code                  VARCHAR(2500),
    exit_message               VARCHAR(2500),
    last_updated               TIMESTAMP WITHOUT TIME ZONE,
    job_configuration_location VARCHAR(2500),
    PRIMARY KEY (job_execution_id),
    FOREIGN KEY (job_instance_id) REFERENCES batch_job_instance(job_instance_id)
);

INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200001, 2, 100001, '2021-02-22 12:00:00', '2021-02-22 12:00:05', '2021-02-22 12:05:00', 'COMPLETED', 'COMPLETED', null, '2021-02-22 12:05:00', '2021-02-22 12:05:00');
INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200002, 2, 100002, '2021-02-22 14:00:00', '2021-02-22 14:00:05', '2021-02-22 14:05:00', 'COMPLETED', 'COMPLETED', null, '2021-02-22 14:05:00', '2021-02-22 14:05:00');
INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200003, 2, 100003, '2021-02-23 14:00:00', '2021-02-23 14:00:05', '2021-02-23 14:05:00', 'COMPLETED', 'COMPLETED', null, '2021-02-23 14:05:00', '2021-02-23 14:05:00');
INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200004, 2, 100004, '2021-02-24 14:00:00', '2021-02-24 14:00:05', '2021-02-24 14:05:00', 'COMPLETED', 'COMPLETED', null, '2021-02-24 14:05:00', '2021-02-24 14:05:00');
INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200005, 2, 100005, '2021-02-25 14:00:00', '2021-02-25 14:00:05', '2021-02-25 14:03:00', 'UNKNOWN', 'UNKNOWN', null, '2021-02-25 14:05:00', '2021-02-26 14:05:00');
INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200006, 2, 100006, '2021-02-26 14:00:00', '2021-02-26 14:00:05', null, 'STARTED', 'EXECUTING', null, '2021-02-26 14:05:00', '2021-02-26 14:05:00');
INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200007, 2, 100007, '3000-02-26 14:00:00', '3000-02-26 14:00:05', null, 'STARTED', 'EXECUTING', null, '3000-02-26 14:05:00', '3000-02-26 14:05:00');
INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200008, 2, 100008, '2021-02-27 14:00:00', '2021-02-27 14:00:05', '2021-02-27 14:05:00', 'FAILED', 'FAILED', null, '2021-02-27 14:05:00', '2021-02-27 14:05:00');
INSERT INTO batch_job_execution(job_execution_id, version, job_instance_id, create_time, start_time, end_time, status, exit_code, exit_message, last_updated, job_configuration_location)
    VALUES (200081, 2, 100008, '2021-02-27 15:00:00', '2021-02-27 15:00:05', '2021-02-27 15:05:00', 'COMPLETED', 'COMPLETED', null, '2021-02-27 15:05:00', '2021-02-27 15:05:00');

CREATE TABLE IF NOT EXISTS batch_job_execution_params
(
    job_execution_id           BIGINT NOT NULL,
    type_cd                    VARCHAR(6) NOT NULL,
    key_name                   VARCHAR(100) NOT NULL,
    string_val                 VARCHAR(250),
    date_val                   TIMESTAMP WITHOUT TIME ZONE,
    long_val                   BIGINT,
    double_val                 DOUBLE PRECISION,
    identifying                CHARACTER(1) NOT NULL,
    FOREIGN KEY (job_execution_id) REFERENCES batch_job_execution(job_execution_id)
);

INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200001, 'DATE', 'timestamp', null, '2021-02-22 12:00:00', null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200002, 'STRING', 'a-param', 'a2', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200002, 'STRING', 'b-param', 'b2', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200003, 'STRING', 'a-param', 'a3', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200003, 'STRING', 'b-param', 'b3', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200004, 'STRING', 'a-param', 'a4', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200004, 'STRING', 'b-param', 'b4', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200005, 'DATE', 'timestamp', null, '2021-02-25 12:00:00', null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200006, 'DATE', 'timestamp', null, '2021-02-26 12:00:00', null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200007, 'DATE', 'timestamp', null, '3000-02-26 12:00:00', null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200008, 'STRING', 'a-param', 'a8', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200008, 'STRING', 'b-param', 'b8', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200081, 'STRING', 'a-param', 'a8', null, null, null, 'Y');
INSERT INTO batch_job_execution_params(job_execution_id, type_cd, key_name, string_val, date_val, long_val, double_val, identifying)
    VALUES (200081, 'STRING', 'b-param', 'b8', null, null, null, 'Y');

CREATE TABLE IF NOT EXISTS batch_job_execution_context
(
    job_execution_id           BIGINT NOT NULL,
    short_context              VARCHAR(2500) NOT NULL,
    serialized_context         TEXT,
    FOREIGN KEY (job_execution_id) REFERENCES batch_job_execution(job_execution_id)
);

INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200001, '{"@class":"java.util.HashMap"}', null);
INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200002, '{"@class":"java.util.HashMap"}', null);
INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200003, '{"@class":"java.util.HashMap"}', null);
INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200004, '{"@class":"java.util.HashMap"}', null);
INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200005, '{"@class":"java.util.HashMap"}', null);
INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200006, '{"@class":"java.util.HashMap"}', null);
INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200007, '{"@class":"java.util.HashMap"}', null);
INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200008, '{"@class":"java.util.HashMap"}', null);
INSERT INTO batch_job_execution_context(job_execution_id, short_context, serialized_context)
    VALUES (200081, '{"@class":"java.util.HashMap"}', null);

CREATE TABLE IF NOT EXISTS batch_step_execution
(
    step_execution_id          BIGINT NOT NULL,
    version                    BIGINT NOT NULL,
    step_name                  VARCHAR(100) NOT NULL,
    job_execution_id           BIGINT NOT NULL,
    start_time                 TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    end_time                   TIMESTAMP WITHOUT TIME ZONE,
    status                     VARCHAR(10),
    commit_count               BIGINT,
    read_count                 BIGINT,
    filter_count               BIGINT,
    write_count                BIGINT,
    read_skip_count            BIGINT,
    write_skip_count           BIGINT,
    process_skip_count         BIGINT,
    rollback_count             BIGINT,
    exit_code                  VARCHAR(2500),
    exit_message               VARCHAR(2500),
    last_updated               TIMESTAMP WITHOUT TIME ZONE,
    PRIMARY KEY (step_execution_id),
    FOREIGN KEY (job_execution_id) REFERENCES batch_job_execution(job_execution_id)
);

INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30011, 3, 'a1-step', 200001, '2021-02-22 12:00:05', '2021-02-22 12:01:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-22 12:01:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30012, 3, 'a2-step', 200001, '2021-02-22 12:01:05', '2021-02-22 12:02:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-22 12:02:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30013, 3, 'a3-step', 200001, '2021-02-22 12:02:05', '2021-02-22 12:03:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-22 12:03:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30021, 3, 'b1-step', 200002, '2021-02-22 14:00:05', '2021-02-22 14:01:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-22 14:01:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30022, 3, 'b2-step', 200002, '2021-02-22 14:01:05', '2021-02-22 14:02:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-22 14:02:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30023, 3, 'b3-step', 200002, '2021-02-22 14:02:05', '2021-02-22 14:03:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-22 14:03:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30031, 3, 'b1-step', 200003, '2021-02-23 14:00:05', '2021-02-23 14:01:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-23 14:01:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30032, 3, 'b2-step', 200003, '2021-02-23 14:01:05', '2021-02-23 14:02:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-23 14:02:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30033, 3, 'b3-step', 200003, '2021-02-23 14:02:05', '2021-02-23 14:03:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-23 14:03:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30041, 3, 'b1-step', 200004, '2021-02-24 14:00:05', '2021-02-24 14:01:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-24 14:01:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30042, 3, 'b2-step', 200004, '2021-02-24 14:01:05', '2021-02-24 14:02:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-24 14:02:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30043, 3, 'b3-step', 200004, '2021-02-24 14:02:05', '2021-02-24 14:03:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-24 14:03:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30051, 3, 'b1-step', 200005, '2021-02-25 14:00:05', '2021-02-25 14:01:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-25 14:01:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30052, 3, 'b2-step', 200005, '2021-02-25 14:01:05', '2021-02-25 14:02:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-25 14:02:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30053, 3, 'b3-step', 200005, '2021-02-25 14:02:05', null, 'STARTED', 10, 0, 0, 10, 0, 0, 0, 0, 'EXECUTING', null, '2021-02-24 14:03:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30081, 3, 'import-semesters', 200008, '2021-02-27 14:00:05', '2021-02-27 14:01:05', 'FAILED', 10, 0, 0, 10, 0, 0, 0, 0, 'FAILED', null, '2021-02-27 15:01:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30181, 3, 'import-semesters', 200081, '2021-02-27 15:00:05', '2021-02-27 15:01:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-27 15:02:05');
INSERT INTO batch_step_execution(step_execution_id, version, step_name, job_execution_id, start_time, end_time, status, commit_count, read_count, filter_count, write_count, read_skip_count, write_skip_count, process_skip_count, rollback_count, exit_code, exit_message, last_updated)
    VALUES (30182, 3, 'b2-step', 200081, '2021-02-27 15:01:05', '2021-02-27 15:03:05', 'COMPLETED', 10, 0, 0, 10, 0, 0, 0, 0, 'COMPLETED', null, '2021-02-27 15:03:05');

CREATE TABLE IF NOT EXISTS batch_step_execution_context
(
    step_execution_id          BIGINT NOT NULL,
    short_context              VARCHAR(2500) NOT NULL,
    serialized_context         TEXT,
    FOREIGN KEY (step_execution_id) REFERENCES batch_step_execution(step_execution_id)
);

INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30011, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30012, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30013, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30021, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30022, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30023, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30031, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30032, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30033, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30041, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30042, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30043, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30051, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30052, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30053, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30081, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30181, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
INSERT INTO batch_step_execution_context(step_execution_id, short_context, serialized_context)
    VALUES (30182, '{"@class":"java.util.HashMap","CourseCategoryReader.read.count":10}', null);
